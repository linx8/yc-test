# Your Creative coding test!

Built using:

- Gatsby starter boilerplate
- React
- Sass
- flickity (react-flickity-component)

## To view :

```
npm install
npm start
```

Or to build

`npm run build`

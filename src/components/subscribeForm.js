import PropTypes from "prop-types"
import React, { useState } from "react"

/*
 * The subscribe form component, handles form submission and fields
 * Pass in onSubmit function
 */
const SubscribeForm = ({ onSubmit }) => {
  const [firstName, setFirstName] = useState("")
  const [lastName, setLastName] = useState("")
  const [email, setEmail] = useState("")

  const handleSubmit = e => {
    e.preventDefault()
    onSubmit({ firstName, lastName, email })
  }

  const canSubmit = firstName.length && lastName.length && email.length

  return (
    <form className="c-form" onSubmit={handleSubmit}>
      <ul className="c-form__fields">
        <li className="c-form__field o-flexbox">
          <div className="o-flex-1  u-spacing-right-small@medium">
            <label htmlFor="firstName">First Name</label>
            <input
              type="text"
              name="firstName"
              onChange={e => setFirstName(e.target.value)}
            />
          </div>
          <div className="o-flex-1  u-spacing-left-small@medium">
            <label htmlFor="lastName">Last Name</label>
            <input
              type="text"
              name="lastName"
              onChange={e => setLastName(e.target.value)}
            />
          </div>
        </li>
        <li className="c-form__field">
          <label htmlFor="email">Email</label>
          <input type="email" onChange={e => setEmail(e.target.value)} />
        </li>
      </ul>
      <div className="u-text-right">
        <button disabled={!canSubmit} className="c-button">
          Submit
        </button>
      </div>
    </form>
  )
}

SubscribeForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
}

export default SubscribeForm

import PropTypes from "prop-types"
import React from "react"
import Flickity from "react-flickity-component"

import "flickity/dist/flickity.css"

const FlickityOptions = {
  wrapAround: true,
}

/* Carousel component using flickity
 * Pass in slide data
 */

const Carousel = ({ slides }) => {
  if (!slides) return

  if (typeof window !== "undefined") {
    return (
      <Flickity className="c-carousel" options={FlickityOptions}>
        {slides.map(slide => (
          <div key={slide.heading} className="c-carousel__item">
            <h1 className="u-heading-1">{slide.heading}</h1>
            <p>{slide.content}</p>
          </div>
        ))}
      </Flickity>
    )
  }
  return null
}

Carousel.propTypes = {
  slides: PropTypes.arrayOf(PropTypes.object).isRequired,
}

export default Carousel

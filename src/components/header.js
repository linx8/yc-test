import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { useStaticQuery, graphql } from "gatsby"

const Header = ({ siteTitle }) => {
  const menuData = useStaticQuery(graphql`
    query menuQuery {
      dataJson {
        menu {
          text
          url
        }
      }
    }
  `)

  return (
    <header className="c-header">
      <div className="o-container">
        <div className="o-flexbox-spaced">
          <h1 style={{ margin: 0 }}>
            <Link className="c-logo" to="/">
              {siteTitle}
            </Link>
          </h1>
          <ul className="c-nav">
            {menuData.dataJson?.menu &&
              menuData.dataJson?.menu.map(menuItem => (
                <li key={menuItem.text}>
                  <Link
                    className="c-nav__link"
                    activeClassName="c-nav__link-active"
                    to={menuItem.url}
                  >
                    {menuItem.text}
                  </Link>
                </li>
              ))}
          </ul>
        </div>
      </div>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

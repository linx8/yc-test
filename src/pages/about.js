import React from "react"
import { useStaticQuery, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const AboutPage = () => {
  const pageData = useStaticQuery(graphql`
    query aboutQuery {
      dataJson {
        about {
          heading
          content
        }
      }
    }
  `)

  console.log(pageData.dataJson)

  return (
    <Layout>
      <SEO title="About" />
      <div className="o-content  u-text-center">
        <h1 className="u-heading-1">
          {pageData.dataJson?.about?.heading || "Default heading!"}
        </h1>
        <p>{pageData.dataJson?.about?.content || "Default content!"}</p>
      </div>
    </Layout>
  )
}

export default AboutPage

import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import SubscribeForm from "../components/subscribeForm"
import bgImage from "../images/background.jpg"
import Carousel from "../components/carousel"
import { useStaticQuery, graphql } from "gatsby"

/*
 * This is the home page
 */

const onSubmit = data => {
  // do something with the data
  console.log("submitted!", data)
}

const IndexPage = () => {
  const homeData = useStaticQuery(graphql`
    query homeQuery {
      dataJson {
        home {
          slides {
            heading
            content
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <SEO title="Home" />
      <div
        className="o-background-image"
        style={{ backgroundImage: `url(${bgImage})` }}
      ></div>
      <div className="o-content">
        <div className="o-flexbox">
          <div className="o-flex  o-flex-7">
            <Carousel slides={homeData.dataJson.home?.slides} />
          </div>
          <div className="o-flex  o-flex-5">
            <SubscribeForm onSubmit={onSubmit} />
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default IndexPage
